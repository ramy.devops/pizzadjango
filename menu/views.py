from django.shortcuts import render
from django.http import HttpResponse
from .models import Pizza

# Create your views here.


def index(request):
    pizzas = Pizza.objects.all().order_by('prix')
    # pizzas_name_price = [pizza.nom + " à " + str(pizza.prix) + "€ " for pizza in pizzas]
    # pizza_price = [pizza.prix for pizza in pizzas]
    # pizzas_name_price_str = ', '.join(pizzas_name_price)    
    # return HttpResponse("Les pizzas : " + pizzas_name_price_str)
    return render(request, 'menu/index.html', {'pizzas': Pizza.objects.all().order_by('prix')})