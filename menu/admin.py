from django.contrib import admin
from .models import Pizza


class PizzaAdmin(admin.ModelAdmin):
    list_display = ('nom', 'ingredients', 'prix', 'vegetarienne')
    list_filter = ('vegetarienne',)
    search_fields = ('nom', 'ingredients')
    ordering = ('nom',)
    
    
admin.site.register(Pizza, PizzaAdmin)
# Register your models here.
