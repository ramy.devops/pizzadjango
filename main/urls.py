from re import I
from unicodedata import name
from django.urls import path
from .import views
app_name = 'main'
urlpatterns = [
    path('', views.index, name='index'),
]